import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthenticationService } from './authentication.service';
import { AuthGuard } from './auth-guard';
import { SharedService } from './shared.service';
import { environment } from '../../environments/environment';


@NgModule({
  imports: [
    CommonModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule
  ],
  declarations: [],
  providers: [ AuthenticationService, AuthGuard, SharedService ]
})
export class CoreModule { }
