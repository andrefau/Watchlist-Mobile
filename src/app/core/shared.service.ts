import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {

  movie: any;

  constructor() { }

  setMovie(movie) {
    this.movie = movie;
  }

  getMovie() {
    return this.movie;
  }

}
