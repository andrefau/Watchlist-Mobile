import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './core/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Watchlist';

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  endret(e) {
    if (e.index === 0) {
      this.router.navigate(['']);
      this.title = 'Watchlist';
    } else {
      this.router.navigate(['/randomizer']);
      this.title = 'Randomizer';
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.title = 'Login';
  }
}
