import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MoviePopupComponent } from './movie-popup/movie-popup.component';
import { SharedService } from '../core/shared.service';
import DataSource from 'devextreme/data/data_source';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {
  @ViewChild(MoviePopupComponent) popup: MoviePopupComponent;
  dataSource: any;
  movieList = [  
   {  
      "Actors":"Martin Sheen, Sissy Spacek, Warren Oates, Ramon Bieri",
      "Available":"Plex",
      "Awards":"Nominated for 1 BAFTA Film Award. Another 3 wins.",
      "BoxOffice":"N/A",
      "Country":"USA",
      "DVD":"27 Apr 1999",
      "Director":"Terrence Malick",
      "Genre":"Crime, Drama",
      "Language":"English, Spanish",
      "Metascore":"90",
      "Plot":"An impressionable teenage girl from a dead-end town and her older greaser boyfriend embark on a killing spree in the South Dakota badlands.",
      "Poster":"https://images-na.ssl-images-amazon.com/images/M/MV5BMDcxNjhiOTEtMzQ0YS00OTBhLTkxM2QtN2UyZDMzNzIzNWFlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
      "Production":"Warner Bros. Pictures",
      "Rated":"PG",
      "Ratings":[  
         {  
            "Source":"Internet Movie Database",
            "Value":"7.9/10"
         },
         {  
            "Source":"Rotten Tomatoes",
            "Value":"98%"
         },
         {  
            "Source":"Metacritic",
            "Value":"90/100"
         }
      ],
      "Released":"05 Jan 1974",
      "Response":"True",
      "Runtime":"94 min",
      "Title":"Badlands",
      "Type":"movie",
      "WatchedDate":"2017/05/06 00:00:00",
      "Website":"N/A",
      "Writer":"Terrence Malick",
      "Year":"1973",
      "imdbID":"tt0069762",
      "imdbRating":"7.9",
      "imdbVotes":"50,255"
   },
   {  
      "Actors":"John Haycraft, Christopher Reeve, Anthony Hopkins, Emma Thompson",
      "Available":"Google Play",
      "Awards":"Nominated for 8 Oscars. Another 16 wins & 24 nominations.",
      "BoxOffice":"N/A",
      "Country":"USA, UK",
      "DVD":"N/A",
      "Director":"James Ivory",
      "Genre":"Drama, Romance",
      "Language":"English, French, German",
      "Metascore":"84",
      "Plot":"A butler who sacrificed body and soul to service in the years leading up to World War II realizes too late how misguided his loyalty was to his lordly employer.",
      "Poster":"https://images-na.ssl-images-amazon.com/images/M/MV5BNDYwOThlMDAtYWUwMS00MjY5LTliMGUtZWFiYTA5MjYwZDAyXkEyXkFqcGdeQXVyNjY1NTQ0NDg@._V1_SX300.jpg",
      "Production":"Sony Pictures Home Entertainment",
      "Rated":"PG",
      "Ratings":[  
         {  
            "Source":"Internet Movie Database",
            "Value":"7.9/10"
         },
         {  
            "Source":"Rotten Tomatoes",
            "Value":"97%"
         },
         {  
            "Source":"Metacritic",
            "Value":"84/100"
         }
      ],
      "Released":"19 Nov 1993",
      "Response":"True",
      "Runtime":"134 min",
      "Title":"The Remains of the Day",
      "Type":"movie",
      "Website":"N/A",
      "Writer":"Kazuo Ishiguro (novel), Ruth Prawer Jhabvala (screenplay)",
      "Year":"1993",
      "imdbID":"tt0107943",
      "imdbRating":"7.9",
      "imdbVotes":"48,151"
   }
];

  constructor(private router: Router, private sharedService: SharedService) { }

  ngOnInit() {
    this.dataSource = new DataSource({
        store: this.movieList,
        searchOperation: 'contains',
        searchExpr: 'Title'
    });
  }

  search(e) {
      this.dataSource.searchValue(e.value);
      this.dataSource.load();
    }

  getTitle(movie: any) {
    const year = new Date(movie.Released).getFullYear();
    const maxLength = 18;
    let title = movie.Title;

    if (title.length > maxLength) {
      title = movie.Title.substring(0, maxLength);
      title += '...';
    }

    return title + ' (' + year + ')';
  }

  showDetails(e) {
    // this.popup.showDetails(e.itemData);
    this.sharedService.setMovie(e.itemData);
    this.router.navigate(['details']);
  }

  add() {
    console.log('add!');
  }
}
