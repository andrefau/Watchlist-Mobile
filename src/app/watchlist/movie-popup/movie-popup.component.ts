import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movie-popup',
  templateUrl: './movie-popup.component.html',
  styleUrls: ['./movie-popup.component.css']
})
export class MoviePopupComponent implements OnInit {

  showPopup = false;
  movie: any = {};
  availableOptions = ['Not available', 'Google Play', 'Netflix', 'Plex'];
  now = new Date();

  constructor() { }

  ngOnInit() {
  }

  showDetails(movie) {
    this.movie = movie;
    this.showPopup = true;
  }

}
