import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../core/authentication.service';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  loginWithEmail(event, email, password) {
    event.preventDefault();
    this.authenticationService.afAuth.auth.signInWithEmailAndPassword(email, password).then((data) => {
      this.router.navigate(['']);
    })
    .catch((error: any) => {
      notify(error.message);
    });
}

}
