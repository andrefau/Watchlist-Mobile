import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from './core/core.module';
import 'devextreme-intl';
import 'hammerjs';
import 'rxjs/Rx';

import {MdToolbarModule,
        MdIconModule,
        MdMenuModule,
        MdButtonModule,
        MdTabsModule,
        MdTabNavBar,
        MdInputModule } from '@angular/material';

import { DxTemplateModule,
         DxListModule,
         DxTextBoxModule,
         DxPopupModule,
         DxSelectBoxModule,
         DxDateBoxModule,
         DxButtonModule } from 'devextreme-angular';

import { AppComponent } from './app.component';
import { WatchlistComponent } from './watchlist/watchlist.component';
import { RandomizerComponent } from './randomizer/randomizer.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './core/auth-guard';
import { MoviePopupComponent } from './watchlist/movie-popup/movie-popup.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';

@NgModule({
  declarations: [
    AppComponent,
    WatchlistComponent,
    RandomizerComponent,
    LoginComponent,
    MoviePopupComponent,
    MovieDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      { path: '', component: WatchlistComponent, canActivate: [AuthGuard] },
      { path: 'randomizer', component: RandomizerComponent, canActivate: [AuthGuard] },
      { path: 'details', component: MovieDetailsComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: '**', redirectTo: '', component: WatchlistComponent, canActivate: [AuthGuard] }
    ]),
    CoreModule,
    MdToolbarModule,
    MdMenuModule,
    MdButtonModule,
    MdIconModule,
    MdTabsModule,
    MdInputModule,
    DxListModule,
    DxTemplateModule,
    DxTextBoxModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxDateBoxModule,
    DxButtonModule
  ],
  providers: [MdTabNavBar],
  bootstrap: [AppComponent]
})
export class AppModule { }
