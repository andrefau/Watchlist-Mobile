import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../core/shared.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  movie: any;
  availableOptions = ['Not available', 'Google Play', 'Netflix', 'Plex'];
  now = new Date();

  constructor(private sharedService: SharedService, private router: Router) { }

  ngOnInit() {
    this.movie = this.sharedService.getMovie();
    if (!this.movie) {
      this.movie = {};
    }
  }

  return() {
    this.router.navigate(['']);
  }

}
