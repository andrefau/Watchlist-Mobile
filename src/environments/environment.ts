// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB2uFI65gjE84ikTTepZoLrdp9o6TZazpM',
    authDomain: 'watchlist-mobile.firebaseapp.com',
    databaseURL: 'https://watchlist-mobile.firebaseio.com',
    projectId: 'watchlist-mobile',
    storageBucket: 'watchlist-mobile.appspot.com',
    messagingSenderId: '97616638742'
  }

};
