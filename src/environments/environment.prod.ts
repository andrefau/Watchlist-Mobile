export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyB2uFI65gjE84ikTTepZoLrdp9o6TZazpM',
    authDomain: 'watchlist-mobile.firebaseapp.com',
    databaseURL: 'https://watchlist-mobile.firebaseio.com',
    projectId: 'watchlist-mobile',
    storageBucket: 'watchlist-mobile.appspot.com',
    messagingSenderId: '97616638742'
  }
};
