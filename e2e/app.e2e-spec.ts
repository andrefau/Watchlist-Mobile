import { WatchlistMobilePage } from './app.po';

describe('watchlist-mobile App', () => {
  let page: WatchlistMobilePage;

  beforeEach(() => {
    page = new WatchlistMobilePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
